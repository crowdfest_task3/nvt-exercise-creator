
<?php

include 'credentials.php';
if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0) {
    echo "non post";
    throw new Exception('Request method must be POST!');
}


if (isset($_POST["BtnInsert"])) {
    $key = $_POST['keyword'];
    $static = $_POST['staticSent'];
    $rel = $_POST['relation'];
    $static = $_POST['staticSent'];
    $maxConceptNetPages = $_POST['maxpages'];
    $level = $_POST['clevel'];
    $mycategory = $_POST['category'];
    $lang = $_POST['lang'];




    $conn = mysql_connect("localhost", $musername, $mpassword); // Connect to the database	
    if (!$conn) {
        echo('Could not connect: ' . mysql_error() . die());
    } else {
        mysql_select_db('nvt');

        $results = get_conceptnet($key, $rel, $static, $maxConceptNetPages, $lang);
        $objects = $results[0];
        $filtered_objects = $results[1];


        //debug_to_console($objects);
        $sql = "INSERT INTO tbl_exercises (category,level,exercise,relation,subject,object,filter_object,remove_for_test,lang) VALUES ('$mycategory','$level','$static','$rel','$key','$objects','$filtered_objects','','$lang')";
//echo $sql;
        $result0 = mysql_query("SET NAMES utf8", $conn);
        $result = mysql_query($sql, $conn);

        echo mysql_errno($conn) . ": " . mysql_error($conn) . "\n";

        //echo var_dumb($results[2]); //number of (clean) terms added to the db
    }

    mysql_close($conn);
}

function get_conceptnet($key, $rel, $static, $maxConceptNetPages, $lang) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $key = mb_strtolower($key, 'UTF-8');

    ini_set('max_execution_time', 300000);
    $maxPages = $maxConceptNetPages;


    $searchtext = conceptnet_search_for_proper_uri($key, $lang);
    $offset = 0;  //Conceptnet has a limitation of 1000 records in its API, we use the offeset
    $counter = 0;
    $mydata = array();

    $myCleanData = array();
    $myCleanDataCounter = 0;

    do {
        $counter = 0;
        $c_api = "http://api.conceptnet.io" . ($searchtext) . "?other=/c/" . $lang . "&limit=1000&offset=" . $offset;

        $data = json_decode(file_get_contents($c_api));
//print_r($data);

        if ($data->error->status != "") {
            echo "Error for: " . $c_api;
        } else {
            $count = 1;

            $arraycounter = 0;
            foreach ($data as &$edges) {
                if (is_array($edges)) {
                    foreach ($edges as $edge) {

                        $arg1 = $edge->start->label;
                        $arg1_lang = $edge->start->language;
                        $arg2 = $edge->end->label;
                        $arg2_lang = $edge->end->language;
                        $relation = $edge->rel->label;
                        echo $arg2 . ",";
                        $ins2 = array();
                        $ins2['key'] = $key;
                        //$ins2['arg1'] = conceptnet_argument_to_normal_text($arg1);
                        //$ins2['arg2'] = conceptnet_argument_to_normal_text($arg2);
                        $ins2['arg1'] = ($arg1);
                        $ins2['arg2'] = ($arg2);
                        //Find the searchable part of the rule
                        //Discard the part that the rule has the name of the country
                        if ((mb_strtolower($key, 'UTF-8') !== mb_strtolower($ins2['arg1'], 'UTF-8')) && (mb_strtolower($key, 'UTF-8') !== mb_strtolower($ins2['arg2'], 'UTF-8'))) {
                            $ins2['searchable_part'] = $ins2['arg1'] . " " . $ins2['arg2'];
                        } elseif (mb_strtolower($key, 'UTF-8') !== mb_strtolower($ins2['arg1'], 'UTF-8')) {
                            $ins2['searchable_part'] = $ins2['arg1'];
                        } else {
                            $ins2['searchable_part'] = $ins2['arg2'];
                        }

                        $ins2['searchable_part_filtered'] = "";
                        $ins2['arg1_full'] = $arg1;
                        $ins2['arg2_full'] = $arg2;
                        $ins2['relation'] = $relation;
                        $ins2['source'] = "conceptnet";
                        $ins2['created_date'] = time();
                        //Make sure that the country name is not stored in the search parameter
                        //Catces cases like <arg1>=<arg2>=country name
                        /*if (mb_strtolower($key, 'UTF-8') !== mb_strtolower($ins2['searchable_part'], 'UTF-8')) {
                            $ins2['active'] = 1;
                        } else {
                            $ins2['active'] = 0;
                        }*/
                        //$db->Autoexecute('geo_rules', $ins2, 'INSERT');


                        if ($relation == $rel) {
                            //debug_to_console( $ins2['relation']);
                            //debug_to_console( $arg1."---".$arg2);

                            $object = "";

                            $object = $ins2['searchable_part'];
                            /* if ($currentvalue !== $key) {
                              $object = $arg1;
                              } else if ($arg2 !== $key) {
                              $object = $arg2;
                              } */



                            $currentvalue = mysql_real_escape_string($object);
                            //$currentvalue = ($object);
                            $mydata[] = $currentvalue; //keep all the characters

                            if ($lang === 'en') {
                                if (!preg_match("/[^A-Za-z0-9]/", $currentvalue)) { //keep only English words
                                    //Make sure that we allow only the selected language words
                                    if ($arg1_lang === $lang && $arg2_lang === $lang) {

                                        //remove the stopwords
                                        $stopwords = array("or", "and", "the", "a");
                                        foreach ($stopwords as &$word) {
                                            $word = '/\b' . preg_quote($word, '/') . '\b/';
                                        }
                                        $currentvalue = preg_replace($stopwords, '', $currentvalue); //clean text without stopwords

                                        $currentvalue = mb_strtolower($currentvalue, 'UTF-8');

                                        if (!empty($currentvalue)) {
                                            if (($currentvalue) !== $key)
                                                $myCleanData[] = $currentvalue;
                                            $myCleanDataCounter++;
                                        }
                                    }
                                }
                            } else {
                                //this is temporary until we find what to do with the rest of the languages
                                if ($arg1_lang === $lang && $arg2_lang === $lang) {
                                    //echo $currentvalue;
                                    $myCleanData[] = $currentvalue;
                                    $myCleanDataCounter++;
                                }
                            }
                        }





                        $arraycounter++;
                        //debug_to_console($object);





                        $counter++;


                        //unset($ins2);
                    }//foreach
                }
            }
            $offset = $offset + $counter;
        }
    } while (($counter === $maxPages));
    //remove duplicate values from array
    $myCleanData = array_unique($myCleanData);

    return array(implode("|", $mydata), implode("|", $myCleanData), $myCleanDataCounter);
}

function conceptnet_search_for_proper_uri($countryname, $lang = 'en') {
    //$c_api = "http://conceptnet5.media.mit.edu/data/5.4/uri?language=en&text=" . urlencode(utf8_encode($countryname));
    //New URL for conceptnet 5.6
    $c_api = "http://api.conceptnet.io/uri?language=" . $lang . "&text=" . urlencode(($countryname));
    $data = json_decode(file_get_contents($c_api), true);
//print_r($data);
//echo $data['@id'];
    //return $data['@context']->uri;
    return $data['@id'];
}

function conceptnet_argument_to_normal_text($argument) {
    $t = explode("/", $argument);
    $num = count($t);
    if ($t[$num - 1] === "n") {
        $selected = $t[$num - 2];
    } else {
        $selected = $t[$num - 1];
    }
    return trim(str_replace("_", " ", $selected));
}

function debug_to_console($data) {
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);

    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}

?>